﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterFallEffectManager : MonoBehaviour
{
    public float distance;
    public Vector3 hitPoint;
    public static WaterFallEffectManager instance;

    public ParticleSystem waterFallParticle;

    public Vector3 pissSideVel;
    
    // Start is called before the first frame update
    void Start()
    {
        instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        distance = Vector3.Distance(hitPoint, transform.position);
        //waterFallParticle.startSpeed = distance * 3.4f / 10;
        float angle = Mathf.Lerp(23.5f, 0, (distance - 9) / (17f - 9));
        waterFallParticle.transform.localRotation = Quaternion.Euler(angle,0,0);

        //var vel = waterFallParticle.velocityOverLifetime;
        //vel.xMultiplier = pissSideVel.x * 50f;
        //vel.zMultiplier = pissSideVel.z * 50f;

        this.transform.rotation = Quaternion.LookRotation(hitPoint - transform.position, Vector3.Cross((hitPoint - transform.position), new Vector3(5,0,0)));
    }
}
