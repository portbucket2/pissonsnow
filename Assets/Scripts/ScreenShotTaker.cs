﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScreenShotTaker : MonoBehaviour
{

    public ParticleSystem penParticle;
    public Vector3 particlePos;
    public Camera rayCam;
    public Vector3 dummyPos;
    float emissionRate;
    bool goEmit;
    public Button resetButton;
    public ParticleSystem smokeParticle;

    public GameObject waterPot;
    public ParticleSystem waterFall;
    float tilt;

    public Vector3 mouseOffset;

    float pissingTimer;

    public float maxPissingTime;

    public Slider greenSlider;
    bool getOffset;
    // Start is called before the first frame update
    void Start()
    {
        //dummyPos = penParticle.transform.localPosition;
    }

    // Update is called once per frame
    void Update()
    {
        Application.targetFrameRate = 60;
        //penParticle.transform.localPosition = Vector3.Lerp(penParticle.transform.localPosition,particlePos, Time.deltaTime * 20);
        //penParticle.transform.localPosition = particlePos;
        waterPot.transform.localRotation = Quaternion.Euler(0, tilt, 0);

        Raycasting();
        var emission = penParticle.emission;
        var smokeEmission = smokeParticle.emission;
        emission.rateOverDistance = emissionRate;
        emission.rateOverTime = emissionRate / 2;
        smokeEmission.rateOverDistance = emissionRate/4;
        waterFall.emissionRate = 60 * emissionRate;

        greenSlider.value = pissingTimer / maxPissingTime;
        if (goEmit)
        {
            
            //penParticle.transform.localPosition = Vector3.Lerp(penParticle.transform.localPosition, particlePos, Time.deltaTime * 20);
            emissionRate = 10;
            tilt = Mathf.Lerp(tilt, 50 , Time.deltaTime * 15f);


            
        }
        else
        {
            
            //penParticle.transform.localPosition = particlePos;
            emissionRate = 0;
            tilt = Mathf.Lerp(tilt, 0, Time.deltaTime * 15f);
        }
        



    }

    public void Raycasting()
    {
        if (Input.GetMouseButton(0))
        {
            RaycastHit hit;

            Vector3 mousePos = Input.mousePosition + mouseOffset;
           // Vector3 mousePos = Input.mousePosition;

            var ray = rayCam.ScreenPointToRay(mousePos);

            if (Physics.Raycast(ray, out hit))
            {
                //if (!getOffset)
                //{
                //    getOffset = true;
                //    mouseOffset = -particlePos + (hit.point);
                //    mouseOffset.y = 0;
                //}
                if (Mathf.Abs(particlePos.x) <= 4.5f && Mathf.Abs(particlePos.z) <= 4.5f)
                {
                    //particlePos = (hit.point) - mouseOffset;
                    dummyPos = hit.point;
                    if (!goEmit)
                    {
                        particlePos = dummyPos;
                    }
                    particlePos = Vector3.Lerp(particlePos, dummyPos, Time.deltaTime * 10);
                    //particlePos = (hit.point);
                    smokeParticle.transform.position = particlePos;
                    Debug.DrawRay(hit.point, transform.up, Color.green);
                    //dummyPos = particlePos;
                    penParticle.transform.localPosition = particlePos;
                    //penParticle.transform.localPosition = Vector3.Lerp(penParticle.transform.localPosition, particlePos, Time.deltaTime * 10);
                    pissingTimer += Time.deltaTime;
                    if (pissingTimer > maxPissingTime)
                    {
                        //Invoke("GoEmit", 0.5f);
                        GoEmit();
                    }
                    
                    //GoEmit();

                    WaterFallEffectManager.instance.hitPoint = particlePos;
                }
                else if (hit.transform.gameObject.layer == 9 || hit.transform.gameObject.layer == 10)
                {
                    //particlePos = (hit.point) - mouseOffset;
                    dummyPos = hit.point;
                    particlePos = Vector3.Lerp(particlePos, dummyPos, Time.deltaTime * 10);
                    //particlePos = (hit.point);
                    smokeParticle.transform.position = particlePos;
                    Debug.DrawRay(hit.point, transform.up, Color.green);
                    //dummyPos = particlePos;
                    penParticle.transform.localPosition = particlePos;
                    //penParticle.transform.localPosition = Vector3.Lerp(penParticle.transform.localPosition, particlePos, Time.deltaTime *10);
                    //GoEmit();
                    goEmit = false;
                    pissingTimer = 0;

                    WaterFallEffectManager.instance.hitPoint = particlePos;
                }
                //else if(Mathf.Abs( particlePos.x ) <= 4.5f && Mathf.Abs(particlePos.z) <= 4.5f)
                //{
                //
                //}




            }
            else
            {
                //emissionRate = 0;
            }
        }
        else
        {
            goEmit = false;
            pissingTimer = 0;
            getOffset = false;
        }
    }

    public void ManipulateParticles()
    {
        dummyPos = new Vector3(particlePos.x , dummyPos.y , particlePos.z);
        
    }

    public void GoEmit()
    {
        if (!goEmit)
        {
            goEmit = true;
        }
    }

    public void ResetParticle()
    {
        penParticle.Clear();
    }



}
